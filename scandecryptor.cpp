/*

Drive Binary Patcher v0.1 g3nuin3 && hunter
This tool will list, find and replace the internal strings used in
all variants of the drive DDoS malware. Use "d" option to decrypt
strings, then use decrypted string in the ini file for the find/replace
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <stdint.h>
#include "ini.h"

using namespace std;

#define INVALID_OFFSET ((size_t)-1)

struct Patterns
{
    vector<string> finds;
    vector<string> replacements;
};

// Drive variant string decryption algorithm
void decrypt(const char* s, char* d, size_t len)
{
    const int clen = len;
    int index = 0;

    char ss[1024] = {0};
    char key = 1;

    while(len)
    {
        if(index % 2 == 0)
            ss[index] = s[index] - key -1;
        else
            ss[index] = s[index] + key +1;

        index++;
        len--;
        if(key==3)
            key =0;
        key++;

    }
    reverse(ss,ss+clen);
    ss[clen+1] = '\n';
    strcpy(d,ss);

}

// Drive variant string encryption algorithm
void encrypt(const char* s, char* d, size_t len)
{

    const int clen = len;
    memcpy(d, s, clen);
    reverse(d,d+clen);
    int index= 0;

    char ss[1024] = {0};
    char key =1;

    while(len)
    {

        if(index %2 == 0)
            ss[index] = d[index] + key +1;
        else
            ss[index] = d[index] - key -1;

        index++;
        len--;
        if(key ==3) key = 0;

        key++;
    }

    ss[clen+1] = '\n';
    strcpy(d,ss);
}

// scan the file for the 4 FFs header and return the offset
size_t find_header(const vector<unsigned char> &bin, size_t pos)
{
    size_t maxlen = bin.size()-4;
    for(size_t i = pos;i < maxlen;i++)
    {
        if(bin[i] == 0xFF && *(const uint32_t*)&bin[i] == 0xFFFFFFFF)
        {   
            return i;
        }
    }
    return INVALID_OFFSET;
}

int handler(void* user, const char* section, const char* name, const char* value)
{
    Patterns *patterns = (Patterns*)user;
    
    if(strcmp(name, "find") == 0) 
    {
        patterns->finds.push_back(value);
    } 
    else if(strcmp(name, "replace") == 0) 
    {
        patterns->replacements.push_back(value);
    } 
    else 
    {
        printf("Unknown key encountered: %s\n", name);
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

bool load_ini(const char *file, Patterns &patterns)
{
    int r = ini_parse(file, handler, &patterns);
    if(r < 0)
    {
        printf("Can't load '%s'\n", file);
        return false;
    } 
    else if(patterns.finds.size() != patterns.replacements.size())
    {
        printf("Unbalanced find and replace pairs!\n");
        return false;
    }
    return true;
}

void list_strings(const char *file)
{

	ifstream f(file, ios::binary);
	if (f)
    {
		f.seekg(0, ios::end);
		streampos len = f.tellg();
		f.seekg(0, ios::beg);

		vector<unsigned char> buffer(len);
		f.read((char*)&buffer[0], len);

		size_t pos = 0;
        int numstr = 0;
        cout << "String Dump Drive Tool: g3nuin3\n\n"<<endl;
		while ((pos = find_header(buffer, pos)) != INVALID_OFFSET)
		{
			char res[256] = { 0 };
			char orig[256] = { 0 };

            uint32_t length = buffer[pos+4];
            memcpy(orig, &buffer[pos+8], length);
            //get decrypted string
            decrypt((const char*)&buffer[pos+8], res, length);
            string newstr = res;
            cout << "Original String: " << orig << "\n" << "Length: " << length \
            <<"\n" << "Decrypted String: " <<  newstr << "\n" << endl;
            
            pos++;
		}
	}
    else
    {
        printf("open file error\n");
        f.close();
    }
}

void process_file(const char *patternsfile, const char *file)
{
    ifstream f(file, ios::binary);
    Patterns patterns;
    if(f && load_ini(patternsfile, patterns))
    {
        f.seekg(0, ios::end);
        streampos len = f.tellg();
        f.seekg(0, ios::beg);

        vector<unsigned char> buffer(len);
        f.read((char*)&buffer[0], len);
        
        bool haserror = false;
        int replaced = 0;
        size_t pos = 0;
        while((pos = find_header(buffer, pos)) != INVALID_OFFSET)
        {
            char res[256] = {0};
            uint32_t *origlen = (uint32_t*)&buffer[pos+4];

            for(size_t i = 0;i < patterns.finds.size();i++)
            {
                if(patterns.finds[i].size() == *origlen)
                {
                    decrypt((const char*)&buffer[pos+8], res, *origlen);
                    if(res == patterns.finds[i])
                    {
                        cout << "Replacing string at " << pos << " with '" << res << "' with '" 
                        << patterns.replacements[i] << "'" << endl;
                        
                        size_t replacelen = patterns.replacements[i].size();
                        encrypt(patterns.replacements[i].c_str(), res, replacelen);
                        if(replacelen > *origlen)
                        {
                            cout << "ERROR: the replacement length is greater than the original length!" << endl;
                            haserror = true;
                        }
                        else
                        {
                            memset(&buffer[pos+8], 0, *origlen);
                            memcpy(&buffer[pos+8], res, replacelen);
                            *origlen = replacelen;
                            replaced++;
                        }
                    }
                }
            }
            // goto next byte
            pos++;
        }
        if(haserror)
        {
            cout << "There were errors, please fix it" << endl;
        }
        else if(replaced > 0)
        {
            string newfile = string(file) + ".out";
            cout << replaced << " replacements were written to " << newfile<< endl;
            ofstream outf(newfile.c_str(), ios::binary);
            if(outf)
                outf.write((char*)&buffer[0], len);
            else
                cout << "Could not open " << newfile << endl;
        }
        else
        {
            cout << "No patterns were found, try editing patterns.ini" << endl;
        }
    }
    else
    {
        cout << "Could not open " << file << endl;
    }
    f.close();
}

int main(int argc, char** argv)
{
    char choice[2] = {0};
    char res[256] = {0};
    char input[256] = {0};

    if(argc != 3)
    {
        printf("usage: scandec d|estring\n");
        printf("usage: scandec l bin_file.exe\n");
        printf("usage: scandec patterns_file.ini bin_file.exe\n");
        return 0;
    }
    else
    {
        strncpy(choice,argv[1], 1);
        switch(choice[0])
        {
            case 'd':
            {
                decrypt(argv[2], res, strlen(argv[2]));
                printf("Decrypted string: %s\n", res);
                break;
            }
            case 'e':
            {
                encrypt(argv[2], res, strlen(argv[2]));
                printf("Encrypted string: %s\n", res);
                break;
            }
            case 'l':
            {
                list_strings(argv[2]);
                break;
            }
            
            default:
                process_file(argv[1], argv[2]);
        }
    }
}
